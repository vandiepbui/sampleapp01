import EStyleSheet from "react-native-extended-stylesheet";

export default (styles = EStyleSheet.create({
  numberText: {
    fontSize: "$largeText",
    textAlign: "center",
    color: "$redColor"
  },
  wrapperBtn: {
    height: "16rem",
    width: "60rem"
  },
  spaceView: {
    height: "5rem"
  }
}));
