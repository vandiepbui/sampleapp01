import React from "react";
import { Text, View } from "react-native";

import styles from "./styles";

//Import components
import MyButton from "../components/MyButton";
import MyConfirmModal from "../components/MyConfirmModal";
import MyAlert from "../components/MyAlert";
import MyImagePicker from "../components/MyImagePicker";
import MyActivityIndicator from "../components/MyActivityIndicator";

//import icons for test
const ic_btn = require("../data/icons/cart.png");

//declare local data
const confirmText = "Bạn có chắc chắn muốn tăng giá trị number này?";
const alertMessage = "Bạn có chắc chắn muốn giảm giá trị number này?";
const alertTitle = "Xác Nhận";

export default class Example extends React.PureComponent {
  state = {
    number: 0
  };

  _handleButtonEvent = type => {
    switch (type) {
      case "alert": {
        this.myAlert.showAlert();
        break;
      }
      case "modal": {
        this.modalConfirm.showModal();
      }
    }
  };

  _increaseNumber = () => {
    this.modalConfirm.closeModal();

    this.setState({
      number: this.state.number + 1
    });
  };

  _decreaseNumber = () => {
    this.myAlert.closeAlert();
    if (this.state.number > 0) {
      this.setState({
        number: this.state.number - 1
      });
    }
  };

  render() {
    return (
      <View>
        <View style={styles.wrapperBtn}>
          <MyButton
            _onPress={() => this._handleButtonEvent("alert")}
            text="Active Alert"
            bgColor="#56BA47"
            color="#fff"
            disable={false}
            iconBtn={ic_btn}
          />
        </View>
        <View style={styles.spaceView} />
        <View style={styles.wrapperBtn}>
          <MyButton
            _onPress={() => this._handleButtonEvent("modal")}
            text="Active Modal"
            bgColor="#124e5e"
            color="#fff"
            disable={false}
            iconBtn={ic_btn}
          />
        </View>
        <View style={styles.spaceView} />
        <Text style={styles.numberText}>{this.state.number}</Text>
        <View style={styles.spaceView} />
        <MyImagePicker />
        <View style={styles.spaceView} />
        <MyActivityIndicator />

        <MyConfirmModal
          text={confirmText}
          confirm={this._increaseNumber}
          ref={c => {
            this.modalConfirm = c;
          }}
        />
        <MyAlert
          title={alertTitle}
          message={alertMessage}
          confirm={this._decreaseNumber}
          ref={c => {
            this.myAlert = c;
          }}
        />
      </View>
    );
  }
}
