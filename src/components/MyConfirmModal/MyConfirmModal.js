import React from "react";
import { Text, View, Modal } from "react-native";

import styles from "./styles";

//import components
import MyModalHeader from "../MyModalHeader";
import MyButton from "../MyButton";

export default class MyConfirmModal extends React.PureComponent {
  state = {
    modalVisible: false
  };

  closeModal = () => {
    this.setState({
      modalVisible: false
    });
  };

  showModal = () => {
    this.setState({
      modalVisible: true
    });
  };

  render() {
    const confirmText = this.props.text;
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {}}
      >
        <View style={styles.modal}>
          <View style={styles.modalContainer}>
            <MyModalHeader
              title="THÔNG BÁO"
              _onPress={() => this.closeModal()}
            />

            <View style={styles.bodyModal}>
              <Text style={styles.confirmText}>{confirmText || "No Text"}</Text>
            </View>
            <View style={styles.footer}>
              <View style={styles.wrapperBtn}>
                <MyButton
                  text="Hủy"
                  bgColor="#ddd"
                  color="#6c757d"
                  disable={false}
                  _onPress={this.closeModal}
                />
              </View>
              <View style={styles.spaceView} />
              <View style={styles.wrapperBtn}>
                <MyButton
                  text="Xác nhận"
                  bgColor="#007bff"
                  color="#fff"
                  disable={false}
                  _onPress={this.props.confirm}
                />
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
