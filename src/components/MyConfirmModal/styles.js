import EStyleSheet from "react-native-extended-stylesheet";

export default (styles = EStyleSheet.create({
  modal: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.15)"
  },
  modalContainer: {
    width: "50%",
    borderWidth: 1,
    borderRadius: "2rem",
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#ddd",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    backgroundColor: "#f7f7f7"
  },
  bodyModal: {
    padding: "10rem",
    borderBottomWidth: 1,
    borderColor: "$borderColor"
  },
  confirmText: {
    color: "$grayColor",
    fontSize: "$normalText"
  },
  footer: {
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingHorizontal: "10rem",
    marginVertical: "5rem"
  },
  wrapperBtn: {
    width: "30rem"
  },
  spaceView: {
    width: "5rem"
  }
}));
