import EStyleSheet from "react-native-extended-stylesheet";

export default (styles = EStyleSheet.create({
  container: {
    height: "14rem",
    position: "relative",
    justifyContent: "center"
  },
  headerText: {
    fontSize: "$largeText",
    textAlign: "center"
  },
  wrapperIcon: {
    position: "absolute",
    right: "10rem",
    top: "5rem"
  },
  ic_close: {
    width: "4rem",
    height: "4rem"
  }
}));
