import React from "react";
import { Text, View, TouchableOpacity, Image } from "react-native";

import styles from "./styles";

//import images/ icons
const ic_close = require("../../data/icons/close.png");

export default (MyModalHeader = props => {
  const tintColor = props.tintColor ? props.tintColor : "#fff";
  const backgroundColor = props.bgColor ? props.bgColor : "#007bff";
  return (
    <View style={[styles.container, { backgroundColor }]}>
      <Text style={[styles.headerText, { color: tintColor }]}>
        {props.title}
      </Text>
      <TouchableOpacity onPress={props._onPress} style={styles.wrapperIcon}>
        <Image
          source={ic_close}
          style={styles.ic_close}
          resizeMethod="resize"
          tintColor={tintColor}
        />
      </TouchableOpacity>
    </View>
  );
});
