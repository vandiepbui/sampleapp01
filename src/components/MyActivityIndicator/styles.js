import EStyleSheet from "react-native-extended-stylesheet";

export default (styles = EStyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center"
  },

  text: {
    fontSize: "$normalText",
    color: "$grayColor",
    textAlign: "center"
  }
}));
