import React from "react";
import { Text, View, ActivityIndicator } from "react-native";

import styles from "./styles";

export default (MyActivityIndicator = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator size="small" color="#6c757d" />
      <Text style={styles.text}>loading</Text>
    </View>
  );
});
