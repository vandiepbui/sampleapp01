import React from "react";
import { Text, View, Modal } from "react-native";

import styles from "./styles";

//import components
import MyButton from "../MyButton";

export default class MyConfirmModal extends React.PureComponent {
  state = {
    modalVisible: false
  };

  closeAlert = () => {
    this.setState({
      modalVisible: false
    });
  };

  showAlert = () => {
    this.setState({
      modalVisible: true
    });
  };

  render() {
    const title = this.props.title;
    const message = this.props.message;
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {}}
      >
        <View style={styles.modal}>
          <View style={styles.modalContainer}>
            <View style={styles.wrapperTitle}>
              <Text style={styles.titleText}>{title}</Text>
            </View>
            <View style={styles.bodyModal}>
              <Text style={styles.confirmText}>{message || "No message"}</Text>
            </View>
            <View style={styles.footer}>
              <View style={styles.wrapperBtn}>
                <MyButton
                  text="Hủy"
                  bgColor="#f7f7f7"
                  color="#6c757d"
                  border={false}
                  disable={false}
                  _onPress={this.closeAlert}
                />
              </View>
              <View style={styles.spaceView} />
              <View style={styles.wrapperBtn}>
                <MyButton
                  text="Xác nhận"
                  bgColor="#f7f7f7"
                  color="#007bff"
                  border={false}
                  disable={false}
                  _onPress={this.props.confirm}
                />
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
