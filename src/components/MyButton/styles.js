import EStyleSheet from "react-native-extended-stylesheet";

export default (styles = EStyleSheet.create({
  container: {
    width: "100%",
    height: "14rem",
    paddingHorizontal: "3rem",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0.5,
    borderRadius: "2rem"
  },
  iconBtn: {
    width: "6rem",
    height: "6rem",
    marginRight: "2rem"
  },
  btnText: {
    fontSize: "$normalText"
  }
}));
