import React from "react";
import { Text, View, TouchableOpacity, Image } from "react-native";

import styles from "./styles";

export default class MyButton extends React.PureComponent {
  render() {
    const item = this.props;
    const backgroundColor = item.bgColor ? item.bgColor : "#fff";
    const color = item.color ? item.color : "#fff";
    const borderColor = item.border == false ? "#f7f7f7" : "#6c757d";

    return (
      <TouchableOpacity
        style={[styles.container, { backgroundColor, borderColor }]}
        onPress={item._onPress}
        disabled={item.disable}
      >
        {item.iconBtn ? (
          <Image
            source={item.iconBtn}
            style={styles.iconBtn}
            resizeMethod="resize"
            tintColor={item.color}
          />
        ) : null}
        <Text style={[styles.btnText, { color }]}>{item.text}</Text>
      </TouchableOpacity>
    );
  }
}
