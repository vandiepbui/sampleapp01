import React, { Component } from "react";
import {
  Platform,
  View,
  Text,
  Modal,
  Image,
  TouchableOpacity
} from "react-native";

import styles from "./styles";

//import images/ icons
const defaultImage = require("../../data/images/no-image.png");
const camera = require("../../data/icons/camera.png");
const gallery = require("../../data/icons/gallery.png");

import ImagePicker from "react-native-image-picker";

export default class MyImagePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarSource: defaultImage,
      modalVisible: false
    };
  }

  pickFromGallery = () => {
    const options = {
      title: "Select Avatar",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    this.setState({
      modalVisible: false
    });
    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        const source = { uri: response.uri };

        this.setState({
          avatarSource: source
        });
      }
    });
  };

  openCamera = () => {
    const options = {
      title: "Chọn ảnh từ thư viện",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    this.setState({
      modalVisible: false
    });
    //launchImageLibrary
    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        this.setState({
          avatarSource: { uri: response.uri }
        });
      }
    });
  };

  showSelectionBox = () => {
    this.setState({
      modalVisible: true
    });
  };

  closeModal = () => {
    this.setState({
      modalVisible: false
    });
  };

  render() {
    return (
      <View>
        <TouchableOpacity onPress={() => this.showSelectionBox()}>
          <Image source={this.state.avatarSource} style={styles.image} />
        </TouchableOpacity>

        <View>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {}}
          >
            <View style={styles.modal}>
              <View style={styles.modalContainer}>
                <View style={styles.modalWrapper}>
                  <View style={styles.bodyModal}>
                    <View style={styles.oneRow}>
                      <Text style={styles.modalTitle}>Chọn ảnh</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.openCamera()}>
                      <View style={styles.oneRow}>
                        <View style={styles.wrapperIconLeft}>
                          <Image
                            source={camera}
                            style={styles.icon}
                            tintColor="#6c757d"
                          />
                        </View>

                        <Text style={styles.modalSelection}>
                          Chụp ảnh từ camera
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.pickFromGallery()}>
                      <View
                        style={[
                          styles.oneRow,
                          { borderBottomColor: "transparent" }
                        ]}
                      >
                        <View style={styles.wrapperIconLeft}>
                          <Image
                            source={gallery}
                            style={styles.icon}
                            tintColor="#6c757d"
                          />
                        </View>
                        <Text style={styles.modalSelection}>
                          Chọn ảnh từ thư viện
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.footerModal}>
                    <TouchableOpacity onPress={() => this.closeModal()}>
                      <Text style={[styles.modalSelection, styles.cancelBtn]}>
                        HỦY
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </View>
    );
  }
}
