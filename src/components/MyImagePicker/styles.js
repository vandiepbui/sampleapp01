import EStyleSheet from "react-native-extended-stylesheet";

export default (styles = EStyleSheet.create({
  modal: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    backgroundColor: "rgba(0,0,0,0.1)"
  },
  modalContainer: {
    width: "100%"
  },
  modalWrapper: {
    width: "40%",
    alignSelf: "center"
  },
  bodyModal: {
    backgroundColor: "#fff",
    borderRadius: "5rem",
    borderWidth: 1,
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#ddd",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    width: "100%",
    elevation: 1
  },
  footerModal: {
    backgroundColor: "#fff",
    borderRadius: "2rem",
    borderBottomWidth: 0,
    shadowColor: "#ddd",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    width: "100%",
    elevation: 1,
    marginVertical: "5rem"
  },
  oneRow: {
    justifyContent: "center",
    alignItems: "center",
    height: "16rem",
    paddingVertical: "5rem",
    borderBottomWidth: 0.5,
    borderColor: "#ddd",
    position: "relative"
  },
  modalTitle: {
    fontSize: "$largeText",
    color: "#aaaaaa"
  },
  modalSelection: {
    color: "$grayColor",
    fontSize: "$normalText",
    textAlign: "center",
    marginLeft: "5rem"
  },
  wrapperIconLeft: {
    height: "16rem",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    left: "10%",
    top: "5%"
  },
  icon: {
    height: "10rem",
    width: "10rem"
  },
  cancelBtn: {
    padding: "3rem"
  },
  image: {
    height: "45rem",
    width: "45rem",
    alignSelf: "flex-start",
    marginTop: "10rem",
    borderColor: "#ccc",
    borderWidth: 0.5
  }
}));
