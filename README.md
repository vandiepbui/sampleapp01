# React Native Sample Application

This project use React Native to build an sample app which include some common components.

## Notes

**_This project use version 0.59 - react native_**
**_This project support only tablet_**

...

### Installing

You can clone or download zip file to try this project.

In root project location (ex: D:\SampleApp)

Run this command:

**npm install**

then run it in android or ios:

**react-native run-android**

**react-native run-ios**

## References

Please read docs at React Native homepage: https://facebook.github.io/react-native/docs/getting-started

## Versioning

version: 1.0

## Authors

- **VAN DIEP BUI** - _Mobile dev_

## Contact

Every issues please contact me via email: **_vandiepbui96@gmail.com_**
