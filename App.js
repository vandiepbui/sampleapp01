import React from "react";
import { Dimensions, Text, View } from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";

//Import examples
import MyExample from "./src/Example";

//Declare global variables for all app
const deviceWidth = Dimensions.get("window").width;
const remUnit = deviceWidth / 380;

EStyleSheet.build({
  $blueColor: "#007bff",
  $blackColor: "#333333",
  $grayColor: "#6c757d",
  $whiteColor: "#ffffff",
  $greenColor: "##56BA47",
  $redColor: "#ff0000",
  $yellowColor: "#ffec00",
  $warnColor: "#ffc107",
  $successColor: "#28a745",
  $dangerColor: "#dc3545",
  $primaryColor: "#17a2b8",
  $blueDark: "#124e5e",
  $borderColor: "#cccccc",
  $rem: remUnit,
  $smallText: 4.5 * remUnit,
  $normalText: 5.5 * remUnit,
  $largeText: 6 * remUnit
});

export default class App extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <MyExample />
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});
